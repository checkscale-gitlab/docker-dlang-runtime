#!/bin/bash

LDC_VER=${MAJOR}.0.${MINOR}
DMD_VER=0.${MINOR}.${BUILD}

rm -rf /tmp/libs/*
mkdir /tmp/libs/dmd
mkdir /tmp/libs/ldc

cp /usr/local/lib/libdruntime-ldc-debug-shared.so.${LDC_VER} /tmp/libs/ldc/
cp /usr/local/lib/libdruntime-ldc-shared.so.${LDC_VER} /tmp/libs/ldc/

cp /usr/local/lib/libphobos2-ldc-debug-shared.so.${LDC_VER} /tmp/libs/ldc/
cp /usr/local/lib/libphobos2-ldc-shared.so.${LDC_VER} /tmp/libs/ldc/

cp /usr/local/lib/libldc-jit-rt.a /tmp/libs/ldc/

cp /usr/lib/x86_64-linux-gnu/libphobos2.so.${DMD_VER} /tmp/libs/dmd/

