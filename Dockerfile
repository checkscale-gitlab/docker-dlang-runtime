FROM registry.gitlab.com/o.lelenkov/docker-minideb:buster-0.1
MAINTAINER Oleg Lelenkov "o.lelenkov@gmail.com"

ARG MAJOR
ARG MINOR
ARG BUILD

ADD ./libs/ldc /usr/local/lib

RUN cd /usr/local/lib && \
    for lib in "libdruntime-ldc-debug" "libdruntime-ldc" "libphobos2-ldc-debug" "libphobos2-ldc"; do \
        ln -s ${lib}.so.${MAJOR}.0.${MINOR} ${lib}.so.${MINOR}; \
        ln -s ${lib}.so.${MINOR} ${lib}.so; \
    done;

ADD ./libs/dmd /usr/lib/x86_64-linux-gnu

RUN cd /usr/lib/x86_64-linux-gnu \
    && ln -s libphobos2.so.0.${MINOR}.${BUILD} libphobos2.so.0.${MINOR} \
    && ln -s libphobos2.so.0.${MINOR} libphobos2.so

RUN install_packages libcurl4

