all: build

export VERSION=$(shell cat VERSION)
export MAJOR=$(shell cat VERSION | awk -F '.' '{print $$1}')
export MINOR=$(shell cat VERSION | awk -F '.' '{print $$2}')
export BUILD=$(shell cat VERSION | awk -F '.' '{print $$3}')

ifndef CI_REGISTRY_IMAGE
	CI_REGISTRY_IMAGE := "registry.gitlab.com/o.lelenkov/docker-dlang-runtime"
endif

extract:
	@./extractlib.sh

build: extract
	@docker build --rm --build-arg MAJOR=${MAJOR} --build-arg MINOR=${MINOR} \
		--tag=${CI_REGISTRY_IMAGE}:${VERSION} .

release: build
	@docker tag ${CI_REGISTRY_IMAGE}:${VERSION} ${CI_REGISTRY_IMAGE}:latest 

publish: release
	@docker push ${CI_REGISTRY_IMAGE}

