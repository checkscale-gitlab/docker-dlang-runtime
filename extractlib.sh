#!/bin/sh

docker run --rm \
    -v $(pwd)/copy.sh:/tmp/copy.sh:Z \
    -v $(pwd)/libs:/tmp/libs \
    -e VERSION=$VERSION \
    -e MAJOR=$MAJOR \
    -e MINOR=$MINOR \
    -e BUILD=$BUILD \
    registry.gitlab.com/o.lelenkov/docker-dlang-dev:$VERSION /tmp/copy.sh

